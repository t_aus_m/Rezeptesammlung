# Crêpes
~4 Portionen

## Zutaten
* 4 Eier 
* ca.250 g Mehl (4 Schaufeln) 
* Ein Päckchen Vanillezucker  
* Ca. ½ l Milch 
* Ein Priese Salz 
* 20 g Zucker

## Arbeitsschritte
1. Alles bis auf die Milch vermengen und nach und nach Milch hinzugeben, sodass ein flüssiger Teig entsteht. 
1. Den Teig dünn in einer Pfanne braten. 
1. Den Crêpe mit Belag genießen. 